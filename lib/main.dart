import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:window_size/window_size.dart';
import 'homepage.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  if (!kIsWeb) {
    if (Platform.isWindows || Platform.isLinux) {
      setWindowTitle("Rouletinium");
      setWindowMinSize(const Size(600, 400));
    }
}
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rouletinium',
      theme: ThemeData(
        scaffoldBackgroundColor: const Color.fromARGB(255, 42, 42, 42),
        colorScheme: const ColorScheme.dark(
          primary: Colors.blue,
          secondary: Colors.white,
          surface: Colors.blue,
          error: Colors.red,
        ),
        primarySwatch: Colors.blue,
      ),
      home: const HomePage(),
    );
  }
}


